#!/usr/bin/env python
########################################################################
# This library is used for communicating with the GoPiGo.
# http://www.dexterindustries.com/GoPiGo/
# History
# ------------------------------------------------
# Author    Date              Comments
# Karan     30 March 14       Initial Authoring
#           02 July  14       Removed bugs and some features added (v0.9)
#           26 Aug   14       Code commenting and cleanup

'''
## License
 GoPiGo for the Raspberry Pi: an open source robotics platform for the Raspberry Pi.
 Copyright (C) 2015 Dexter Industries

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/gpl-3.0.txt>.
'''
#
########################################################################

import serial, time
import smbus
import math
import struct

import smbus
import time
import subprocess


########################################################################
class GoPiGo(object):
    """"""
    # This is the address for the GoPiGo
    address = 0x08
    fwd_cmd              = [119]        #Move forward with PID
    motor_fwd_cmd        = [105]        #Move forward without PID
    bwd_cmd              = [115]        #Move back with PID
    motor_bwd_cmd        = [107]        #Move back without PID
    debug = 0
    #GoPiGo Commands
    left_cmd             = [97]         #Turn Left by turning off one motor
    left_rot_cmd         = [98]         #Rotate left by running both motors is opposite direction
    right_cmd            = [100]        #Turn Right by turning off one motor
    right_rot_cmd        = [110]        #Rotate Right by running both motors is opposite direction
    stop_cmd             = [120]        #Stop the GoPiGo
    ispd_cmd             = [116]        #Increase the speed by 10
    dspd_cmd             = [103]        #Decrease the speed by 10
    m1_cmd               = [111]        #Control motor1
    m2_cmd               = [112]        #Control motor2
    read_motor_speed_cmd = [114]        #Get motor speed back

    volt_cmd             = [118]        #Read the voltage of the batteries
    us_cmd               = [117]        #Read the distance from the ultrasonic sensor
    led_cmd              = [108]        #Turn On/Off the LED's
    servo_cmd            = [101]        #Rotate the servo
    enc_tgt_cmd          = [50]         #Set the encoder targeting
    fw_ver_cmd           = [20]         #Read the firmware version
    en_enc_cmd           = [51]         #Enable the encoders
    dis_enc_cmd          = [52]         #Disable the encoders
    read_enc_status_cmd  = [53]         #Read encoder status
    en_servo_cmd         = [61]         #Enable the servo's
    dis_servo_cmd        = [60]         #Disable the servo's
    set_left_speed_cmd   = [70]         #Set the speed of the right motor
    set_right_speed_cmd  = [71]         #Set the speed of the left motor
    en_com_timeout_cmd   = [80]         #Enable communication timeout
    dis_com_timeout_cmd  = [81]         #Disable communication timeout
    timeout_status_cmd   = [82]         #Read the timeout status
    enc_read_cmd         = [53]         #Read encoder values
    trim_test_cmd        = [30]         #Test the trim values
    trim_write_cmd       = [31]         #Write the trim values
    trim_read_cmd        = [32]

    digital_write_cmd    = [12]         #Digital write on a port
    digital_read_cmd     = [13]         #Digital read on a port
    analog_read_cmd      = [14]         #Analog read on a port
    analog_write_cmd     = [15]         #Analog read on a port
    pin_mode_cmd         = [16]         #Set up the pin mode on a port

    ir_read_cmd          = [21]
    ir_recv_pin_cmd      = [22]
    cpu_speed_cmd        = [25]

    #LED Pins
    #MAKE COMPATIBLE WITH OLD FIRMWARE
    # LED_L_PIN=17
    # LED_R_PIN=16

    #LED setup
    LED_L=1
    LED_R=0

    # This allows us to be more specific about which commands contain unused bytes
    unused = 0


    v16_thresh=790

    #----------------------------------------------------------------------
    def __init__(self, bus_no=1, address=0x08):
        """Constructor"""
        self.bus_no = bus_no
        self.address = address
        
        self.bus = smbus.SMBus(self.bus_no)
        
        self.version = 14
        
        raw = 0
        for i in range(10):
            raw = self.analogRead(7)
    
        if raw > self.v16_thresh:
            version = 16
        
    def write_i2c_block(self, address, block):
        """Write I2C block."""
        try:
            op = self.bus.write_i2c_block_data(address, 1, block)
            time.sleep(.005)
            return op
        except IOError:
            if self.debug:
                print "IOError"
            return -1
        return 1
    
    def writeNumber(self, value):
        """Write a byte to the GoPiGo"""
        try:
            self.bus.write_byte(self.address, value)
            time.sleep(.005)
        except IOError:
            if self.debug:
                print "IOError"
            return -1
        return 1
    
    def readByte(self):
        """Read a byte from the self."""
        try:
            number = self.bus.read_byte(self.address)
            time.sleep(.005)
        except IOError:
            if self.debug:
                print "IOError"
            return -1
        return number
    
    def motor1(self, direction, speed):
        """Control Motor 1."""
        return self.write_i2c_block(self.address, self.m1_cmd + [direction, speed, 0])
    
    def motor2(self, direction, speed):
        """Control Motor 2."""
        return self.write_i2c_block(self.address, self.m2_cmd + [direction, speed, 0])
    
    def fwd(self):
        """Move the self forward."""
        return self.write_i2c_block(self.address, self.motor_fwd_cmd + [0, 0, 0])
    
    def motor_fwd(self):
        """Move the self forward without PID."""
        return self.write_i2c_block(self.address, self.motor_fwd_cmd + [0, 0, 0])
    
    def bwd(self):
        """Move self back."""
        return self.write_i2c_block(self.address, self.motor_bwd_cmd + [0, 0, 0])
    
    def motor_bwd(self):
        """Move self back without PID control."""
        return self.write_i2c_block(self.address, self.motor_bwd_cmd + [0, 0, 0])
    
    def left(self):
        """Turn self Left slow (one motor off, better control)."""
        return self.write_i2c_block(self.address, self.left_cmd + [0, 0, 0])
    
    def left_rot(self):
        """Rotate self left in same position (both motors moving in the opposite
           direction).
        """
        return self.write_i2c_block(self.address, self.left_rot_cmd + [0, 0, 0])
    
    def right(self):
        """Turn self right slow (one motor off, better control)."""
        return self.write_i2c_block(self.address, self.right_cmd + [0, 0, 0])
    
    def right_rot(self):
        """Rotate self right in same position both motors moving in the opposite
           direction).
        """
        return self.write_i2c_block(self.address, self.right_rot_cmd + [0, 0, 0])
    
    def stop(self):
        """Stop the self."""
        return self.write_i2c_block(self.address, self.stop_cmd + [0, 0, 0])
    
    def increase_speed(self):
        """Increase the speed."""
        return self.write_i2c_block(self.address, self.ispd_cmd + [0, 0, 0])
    
    def decrease_speed(self):
        """Decrease the speed."""
        return self.write_i2c_block(self.address, self.dspd_cmd + [0, 0, 0])
    
    def trim_test(self, value):
        """Trim test with the value specified."""
        if value > 100:
            value = 100
        elif value < -100:
            value = -100
        value += 100
        self.write_i2c_block(self.address, self.trim_test_cmd + [value, 0, 0])
    
    def trim_read(self):
        """Read the trim value in EEPROM if present else return -3."""
        self.write_i2c_block(self.address, self.trim_read_cmd + [0, 0, 0])
        time.sleep(.08)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
    
        if b1 != -1 and b2 != -1:
            v= b1 * 256 + b2
            if v==255:
                return -3
            return v
        else:
            return -1
    
    def trim_write(self, value):
        """Write the trim value to EEPROM, where -100=0 and 100=200."""
        if value > 100:
            value = 100
        elif value < -100:
            value = -100
        value += 100
        self.write_i2c_block(self.address, self.trim_write_cmd + [value, 0, 0])
    
    
    def digitalRead(self, pin):
        """Arduino Digital Read."""
        if pin == 10 or pin == 15 or pin == 0 or pin == 1:
            self.write_i2c_block(self.address, self.digital_read_cmd + [pin, self.unused, self.unused])
            time.sleep(.1)
            n=self.bus.read_byte(self.address)
            self.bus.read_byte(self.address)        #Empty the buffer
            return n
        else:
            return -2
    
    def digitalWrite(self, pin, value):
        """Arduino Digital Write."""
        #if pin ==10 or pin ==0 or pin ==1 or pin==5 or pin ==16 or pin==17 :
        if value== 0 or value == 1:
            self.write_i2c_block(self.address, self.digital_write_cmd + [pin, value, self.unused])
            # time.sleep(.005)    #Wait for 5 ms for the commands to complete
            return 1
        #else:
        #    return -2
    
    def pinMode(self, pin, mode):
        """Setting Up Pin mode on Arduino."""
        # if pin ==10 or pin ==15 or pin ==0 or pin ==1:
        if mode == "OUTPUT":
            self.write_i2c_block(self.address, self.pin_mode_cmd + [pin, 1, self.unused])
        elif mode == "INPUT":
            self.write_i2c_block(self.address, self.pin_mode_cmd + [pin, 0, self.unused])
        #time.sleep(.005)    #Wait for 5 ms for the commands to complete
        return 1
        # else:
            # return -2
    
    def analogRead(self, pin):
        """Read analog value from Pin."""
        self.write_i2c_block(self.address, self.analog_read_cmd + [pin, self.unused, self.unused])
        time.sleep(.007)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
        return b1 * 256 + b2
    
    def analogWrite(self, pin, value):
        """Write PWM."""
        if pin == 10 :
            self.write_i2c_block(self.address, self.analog_write_cmd + [pin, value, self.unused])
            return 1
        else:
            return -2
    
    def volt(self):
        """Read voltage.
    
           return: voltage in V
        """
        self.write_i2c_block(self.address, self.volt_cmd + [0, 0, 0])
        time.sleep(.1)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
    
        if b1 != -1 and b2 != -1:
            v = b1 * 256 + b2
            v = (5 * float(v) /1024) / .4
            return round(v, 2)
        else:
            return -1
    
    def brd_rev(self):
        """Read board revision.
           return: voltage in V
        """
        self.write_i2c_block(self.address, self.analog_read_cmd + [7, self.unused, self.unused])
        time.sleep(.1)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
        return b1 * 256 + b2
    
    def us_dist(self, pin):
        """Read ultrasonic sensor.
            arg:
                pin: Pin number on which the US sensor is connected
           return: distance in cm
        """
        self.write_i2c_block(self.address, self.us_cmd + [pin, 0, 0])
        time.sleep(.08)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
        if b1 != -1 and b2 != -1:
            v = b1 * 256 + b2
            return v
        else:
            return -1
    
    def read_motor_speed(self):
        self.write_i2c_block(self.address, self.read_motor_speed_cmd + [self.unused, self.unused, self.unused])
        try:
            s1 = self.bus.read_byte(self.address)
            s2 = self.bus.read_byte(self.address)
        except IOError:
            return [-1, -1]
        return [s1, s2]
    
    def led_on(self, l_id):
        """Turn led on.
    
            arg: l_id: 1 for left LED and 0 for right LED
        """
        if self.version > 14:
            r_led = 16
            l_led = 17
        else:
            r_led = 5
            l_led = 10
    
        if l_id == self.LED_L or l_id == self.LED_R:
            if l_id == self.LED_L:
                self.pinMode(l_led, "OUTPUT")
                self.digitalWrite(l_led, 1)
            elif l_id==self.LED_R:
                self.pinMode(r_led,"OUTPUT")
                self.digitalWrite(r_led,1)
            return 1
        else:
            return -1
    
    def led_off(self, l_id):
        """Turn led off.
    
           arg: l_id: 1 for left LED and 0 for right LED
        """
        if self.version > 14:
            r_led = 16
            l_led = 17
        else:
            r_led = 5
            l_led = 10
    
        if l_id == self.LED_L or l_id == self.LED_R:
            if l_id == self.LED_L:
                self.pinMode(l_led, "OUTPUT")
                self.digitalWrite(l_led, 0)
            elif l_id == self.LED_R:
                self.pinMode(r_led, "OUTPUT")
                self.digitalWrite(r_led, 0)
            return 1
        else:
            return -1
    
    def servo(self, position):
        """Set servo position
    
           arg:
            position: angle in degrees to set the servo at
        """
        self.write_i2c_block(self.address, self.servo_cmd + [position, 0, 0])
    
    def enc_tgt(self, m1, m2, target):
        """Set encoder targeting on.
    
           arg:
               m1: 0 to disable targeting for m1, 1 to enable it
               m2: 0 to disable targeting for m2, 1 to enable it
               target: number of encoder pulses to target (18 per revolution)
        """
        if m1 > 1 or m1 < 0 or m2 > 1 or m2 < 0:
            return -1
        m_sel= m1 * 2 + m2
        self.write_i2c_block(self.address, self.enc_tgt_cmd + [m_sel, target / 256, target % 256])
        return 1
    
    def enc_read(self, motor):
        """Read encoder value.
    
           arg:
               motor -> 0 for motor1 and 1 for motor2
    
           return:
               distance in cm
        """
        self.write_i2c_block(self.address, self.enc_read_cmd + [motor, 0, 0])
        time.sleep(.08)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
        if b1 != -1 and b2 != -1:
            v = b1 * 256 + b2
            return v
        else:
            return -1
    
    def fw_ver(self):
        """Returns the firmware version."""
        self.write_i2c_block(self.address, self.fw_ver_cmd + [0, 0, 0])
        time.sleep(.1)
        try:
            ver = self.bus.read_byte(self.address)
            self.bus.read_byte(self.address)        #Empty the buffer
        except IOError:
            return -1
        return float(ver) / 10
    
    def enable_encoders(self):
        """Enable the encoders (enabled by default)."""
        return self.write_i2c_block(self.address, self.en_enc_cmd + [0, 0, 0])
    
    def disable_encoders(self):
        """Disable the encoders (use this if you don't want to use the encoders)."""
        return self.write_i2c_block(self.address, self.dis_enc_cmd +[0, 0, 0])
    
    def enable_servo(self):
        """Enables the servo."""
        return self.write_i2c_block(self.address, self.en_servo_cmd +[0, 0, 0])
    
    def disable_servo(self):
        """Disable the servo."""
        return self.write_i2c_block(self.address, self.dis_servo_cmd + [0, 0, 0])
    
    def set_left_speed(self, speed):
        """Set speed of the left motor.
    
           arg: speed-> 0-255
        """
        if speed > 255:
            speed = 255
        elif speed < 0:
            speed = 0
        return self.write_i2c_block(self.address, self.set_left_speed_cmd + [speed, 0, 0])
    
    def set_right_speed(self, speed):
        """Set speed of the right motor
    
           arg: speed-> 0-255
        """
        if speed > 255:
            speed = 255
        elif speed < 0:
            speed = 0
        return self.write_i2c_block(self.address, self.set_right_speed_cmd + [speed, 0, 0])
    
    def set_speed(self, speed):
        """Set speed of the both motors
    
           arg: speed-> 0-255
        """
        if speed > 255:
            speed = 255
        elif speed < 0:
            speed = 0
        self.set_left_speed(speed)
        time.sleep(.1)
        self.set_right_speed(speed)
    
    def enable_com_timeout(self, timeout):
        """Enable communication time-out(stop the motors if no command received
           in the specified time-out).
    
           arg: timeout-> 0-65535 (timeout in ms)
        """
        return self.write_i2c_block(self.address, self.en_com_timeout_cmd + [timeout/256, timeout%256, 0])
    
    def disable_com_timeout(self):
        """Disable communication time-out."""
        return self.write_i2c_block(self.address, self.dis_com_timeout_cmd + [0, 0, 0])
    
    #Read the status register on the self
    #    Gets a byte, b0-enc_status
    #                 b1-timeout_status
    #    Return: list with    l[0]-enc_status
    #                        l[1]-timeout_status
    def read_status(self):
        st = self.bus.read_byte(self.address)
        st_reg = [st & (1 << 0), (st & (1 << 1))/2]
        return st_reg
    
    def read_enc_status(self):
        """Read encoder status.
    
           return: 0 if encoder target is reached
        """
        st = self.read_status()
        return st[0]
    
    def read_timeout_status(self):
        """Read timeout status.
    
           return: 0 if timeout is reached
        """
        st = self.read_status()
        return st[1]
    
    def ir_read_signal(self):
        """ Grove - Infrared Receiver- get the commands received from the Grove IR sensor."""
        try:
            self.write_i2c_block(self.address, self.ir_read_cmd + [self.unused, self.unused, self.unused])
            time.sleep(.1)
            data_back = self.bus.read_i2c_block_data(self.address, 1)[0:21]
            if data_back[1] <> 255:
                return data_back
            return [-1] * 21
        except IOError:
            return [-1] * 21
    
    def ir_recv_pin(self, pin):
        """ Grove - Infrared Receiver- set the pin on which the Grove IR sensor is connected."""
        self.write_i2c_block(self.address, self.ir_recv_pin_cmd + [pin, self.unused, self.unused])
    
    def cpu_speed(self):
        self.write_i2c_block(self.address, self.cpu_speed_cmd + [0, 0, 0])
        time.sleep(.1)
        try:
            b1 = self.bus.read_byte(self.address)
            b2 = self.bus.read_byte(self.address)
        except IOError:
            return -1
        return b1
    
        #Enable slow i2c (for better stability)
        #def en_slow_i2c():
        #    #subprocess.call('sudo rmmod i2c_bcm2708',shell=True)
        #    subprocess.call('sudo modprobe i2c_bcm2708 baudrate=70000',shell=True)



