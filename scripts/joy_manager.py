class JoyManager(object):
    """Manages buttons from a joy topic to debounce them. """

    DEBOUNCE_THRS = 5

    def __init__(self, n):
        self.n = n
        self.pressed = [False] * n
        self.debounce_counter = [0] * n

    def get_events(self, buttons_now):
        events = []
        for i in range(min(self.n, len(buttons_now))):
            if buttons_now[i] != self.pressed[i]:
                self.debounce_counter[i] += 1
                if self.debounce_counter[i] == self.DEBOUNCE_THRS:
                    self.pressed[i] = not self.pressed[i]
                    events.append(i)
            else:
                self.debounce_counter[i] = 0
        return events
