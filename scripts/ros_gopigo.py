#!/usr/bin/python
"""
Simple ROS interface to the GoPiGo using joy messages.
"""
import argparse
import rospy
from sensor_msgs.msg import Joy

import gopigo
from gopigo import GoPiGo

from joy_manager import JoyManager


class DifferentialDrive(object):
    """Simple individual motor control. """

    LEFT_STICK  = 1
    RIGHT_STICK = 4
    DEADMAN = 5

    L_LED_BTN = 2
    R_LED_BTN = 1

    #SERVO_L = 7
    #SERVO_R = 5

    def __init__(self, bus_no=1):
        self.jmanager = JoyManager(20)
        self.gopigo = GoPiGo(bus_no=bus_no)
	rospy.loginfo(bus_no)
        #self.servo_pos = 90
        #self.servo(self.servo_pos)

    def joy_cb(self, data):
        events = self.jmanager.get_events(data.buttons)
        direction_right = 1 if data.axes[self.RIGHT_STICK] > 0 else 0
        direction_left = 1 if data.axes[self.LEFT_STICK] > 0 else 0

        self.gopigo.motor1(direction_right, int(abs(data.axes[self.RIGHT_STICK]) * 255));
        self.gopigo.motor2(direction_left,  int(abs(data.axes[self.LEFT_STICK]) * 255));

        if self.L_LED_BTN in events:
            if self.jmanager.pressed[self.L_LED_BTN]:
                self.gopigo.led_on(1)
            else:
                self.gopigo.led_off(1)

        if self.R_LED_BTN in events:
            if self.jmanager.pressed[self.R_LED_BTN]:
                self.gopigo.led_on(0)
            else:
                self.gopigo.led_off(0)

        #if self.SERVO_L in events:
            #self.servo_pos += 10
            #if self.servo_pos > 180:
                #self.servo_pos = 180

        #if self.SERVO_R in events:
            #self.servo_pos -= 10
            #if self.servo_pos < 0:
                #self.servo_pos = 0

        #if self.SERVO_R in events or self.SERVO_L in events:
            #self.gopigo.enable_servo()
            #self.servo(self.servo_pos)
            #self.gopigo.disable_servo()

if __name__ == '__main__':
    
    rospy.init_node("GoPiGo_Joy")

    parser = argparse.ArgumentParser(description="description")
    parser.add_argument('--bus_no', '-b', type=int, help='', default=1)
    args, unknown = parser.parse_known_args()
    print(args)
    
    drive = DifferentialDrive(bus_no=args.bus_no)
    rospy.Subscriber("/joy", Joy, drive.joy_cb, queue_size=1)
    rospy.loginfo("Spinning")
    rospy.spin()
